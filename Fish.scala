class Fish(val name:String, val sex:Boolean) {
    def presentation():Unit=println("Le poisson se nomme "+name+"et c'est"+{if (sex) "un mâle" else "une femelle"});
}
